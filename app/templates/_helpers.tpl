{{/* Docker registry configuration */}}
{{- define "imagePullSecret" }}
{{- with .Values.registry }}
{{- printf "{\"auths\":{\"%s\":{\"auth\":\"%s\"}}}" .repository .auth  | b64enc }}
{{- end }}
{{- end }}
